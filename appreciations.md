### APPRECIATIONS

I end each day with Appreciations. It's a habit. I speak aloud three items from the day that touched, moved, inspired, challenged or deepened me. Here are some of my global, time-tested appreciations.

#### Digital

+ [Thoreau 2.0](https://static.pinboard.in/xoxo_talk_thoreau.htm): a call to be brave
+ [What Makes Us Happy?](http://www.theatlantic.com/magazine/print/2009/06/what-makes-us-happy/307439/): fascinating, if dense, study of the good life
+ [ANVC: Almost Nonviolent Communication](http://anvc.svenhartenstein.de/en/about/): one of the few things that I've found that gets communication right-ish
+ [Growing is Forever](http://vimeo.com/18305022)

#### Music

+ Kanye West
+ Sage the Gemini

#### Programmers

+ Linus Torvalds: for Git, for Linux, for his funny way of putting things
+ TJ Holowaychuk: for Express, for Jade, for Stylus, for his Git aliases
+ [Dominic Tarr](/dt): for so much. Especially for those few days in Oakland in 2013
+ [Suckless](http://suckless.org): for the no nonsense approach to computing
+ Richard Stallman: for leading the way for years, long before I even knew it existed, in the UNIX/GNU world

#### People

+ Buckminster Fuller
+ [Dr Bronner](http://drbronner.com): earlier this year (2013) I finally read the fine print on the peppermint Bronners soap I prefer (the only cosmetic you need is not an understatement, it's a life choice). Reading the fine print I discovered Dr Bronner's family was killed during the Holocaust. Bronner'd already fled, and though he begged his parents to come with, they refused; the last thing he heard from his family was [a censored postcard](http://www.straightdope.com/classics/a3_386.html) that read: You were right.

#### Movies

+ [Pan's Labyrinth](http://www.rottentomatoes.com/m/pans_labyrinth/): amazing, gorgeous, stunning, heart-moving movie about little girls, faeries and war - perhaps my favorite film
+ Kung Fu Hustle: breathtaking camera work, gorgeous character development, total over the top humor
+ The Dictator: some things are beyond comment. This film is one such film

#### Books

+ A Guide to the Good Life: The Ancient Art of Stoic Joy, [William B. Irvine](http://boingboing.net/2010/11/01/twenty-first-century-4.html)
+ [On the Shortness of Life, Seneca](http://www.us.penguingroup.com/nf/Book/BookDisplay/0,,9781101651186,00.html?On_the_Shortness_of_Life_Seneca)
+ The War of Art, Steven Pressfield: The only book I recommend for people doing any sort of creative work
+ 1984: again, beyond comment. Read it for the first time in November 2013; world-view changer
+ Awakening the Spine, Vanda Scaravelli: The only yoga book I recommend

#### Your Appreciations

> Dear Gwen, I just wanted to tell you that I became a fan today. I love you, but not in a creepy way. I just love your guts for making so many hard choices and being proud of them. 

[Filipe Catraia](http://oliva.io)

> I stopped bullshitting. I learned how to use git. It was hard, and it was worth it. I built this site and connected with people and got some clarity. I also got saddled with an unscratchable itch to learn more code, but that's another story. 

[Madeleine Forbes](http://madeleineforbes.co.uk)
