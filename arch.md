###Put Some Arch in It

Arch Linux gets a 10/10 for freedom in an operating system. So why do so few people talk about it? It's a little like Fight Club, so let's start with the body.

<hr />

Not too many years ago, when I gave the command to arch, I was referring to the body. More often than not, the spine. I was usually talking about taking the arch _out_ of the body, not putting arch in. (Unless crescent moon or another deep arch of that nature.)

In yoga, we engage our abdominals to draw the innards toward the spine, taking the arch, and emphasis, out of the lower back. If you do that in yoga, or even as you're walking or standing today, you might notice your posture improve. That's because it's the abdominals - engaging them - that lengthens and elevates you.

Stand up straight is the wrong way to think about it. Engage your abdominals, that eliminates the arch.

Arch now means this operating system I use daily. The system boots to a black screen with a faint glow. No immediate gui. Blinking cursor. I type in my user name. It responds with a query for my password. If I provide the correct answer I type 'startx' to turn Enlightenment on. 

If this sounds very far out and groovy, that's because it is.

#### Why Arch

I put Arch on my system with the help of the [Beginner's Guide to Arch](https://wiki.archlinux.org/index.php/Beginners%27_Guide), in September 2013 when I moved to Oakland. I'd put it on there before but had added Gnome and it was interfering with Enlightenment. This time, with a fresh install, I'm running the leanest operating system I can find. Arch Linux, Enlightenment and Terminology for the terminal.

#### Why? Because freedom. 

This is the lightest, and therefore most freeing (light because free - engage your abs right now, it'll free up your shoulders and upper body) system I've ever used. When I discovered it has been around for years, I was shocked. It doesn't have a marketing department, and few people who use it talk about using it, so finding it is sort of happening upon a secret kept by thousands of people.

#### How Now

Here's how. If you want to get Arch on your machine, read through the [Beginner's Guide to Arch](https://wiki.archlinux.org/index.php/Beginners%27_Guide). Then, if you have a buddy and you think they'd be willing to hang around during the install, invite them over. If you want me to join you on your mission, [let's talk about that](mailto:gwen@gwenbell.com). 

Sometimes, depending on your current system, it'll take a couple of tries to get it to take. I'm on an Intel PC for the first time since 2000. It took a lot easier on this machine than it did on my Macbook Air (which is now bricked and belongs to the highest bidder who got it off ebay).

If that last parenthetical didn't scare you off, it should. You don't want to go fraking around with Arch if you have no idea what you're doing and you want to be certain of success. Success is not guaranteed. But then, the risk is part of what makes using Arch such a reward.

#### Who Else

The other people who use Arch (including [Dominic Tarr](/dt) who I helped with getting Arch on his box) tend to be the quiet and brilliant types. I'm only putting on my old marketing hat to tell you about this because I think more people should know about it, and I'm not afraid the creator of Arch is going to hunt me down in my sleep. If he does, we can have a dynamic conversation!

#### Updated November 2013 to Add

I now use dwm exclusively; I'm off Gnome, though it's great for a beginner!

<div style="font-size: .6em">Updated 25 November 2013</div>


