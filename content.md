### GWEN BELL

Hi, I'm [Gwen](/about). I'm an expat technical writer. 

#### DAILY

<div style="font-size: 1em">  25 November 2013 </div>

Got up, did my practice. Updated [Appreciations](/appreciations) with Bucky and Bronners. French pressed, tiny sake cup coffee morning of writing ahead! Practiced Spanish conversation (nothing more slangish than 'por supuesto/claro' for 'of course').

#### ALIGN

[Align Your Website](http://align.gwenbell.com)

#### NOV/DEC 2013

+ Run [Gaman](http://gwenbell.com/gaman) for writers/programmers to push daily commits in December
+ Make [Nunu Bell](http://nunubell.com) beautiful and robust for my grandmother

<div style="font-size: .6em">Updated 25 November 2013</div>

