### GAMAN

> Gaman is a Japanese term that means perservering despite difficulty. When life presents you with a challenge, you dig deep and find that gaman spirit to move forward.

Gaman is for writers/programmers to commit to a month of daily commits this December.

#### Participants

1. [Your Name Here](yoursite dot thing) (for participation details, read on...)
1. [Hannah](http://embassyofexploration.com)
1. [Alex J](http://alemja.net)
1. [Bosco](http://boscoh.com)
1. [Paula](http://dentintheworld.com)
1. [Jen Lowe](http://datatelling.com/)
1. [Filipe Catraia](http://www.filipecatraia.com/journal)
1. [Alex Payne](https://al3x.net/)
1. [Tyron](http://tyronlove.com)
1. [Madeleine Forbes](http://madeleineforbes.co.uk)
1. [time2ctw](http://changetheworld.neocities.org)
1. [Dave Lukas](http://davelukas.net)
1. [Ev Bogue](http://evbogue.com)
1. [Gwen Bell](http://gwenbell.com)

#### Description

This year's end of year project is [Gaman](http://en.wikipedia.org/wiki/Gaman_(term). 

Commit one written response to a prompt each day during the month of December. 

It's an opportunity for writers to use Git to manage their writing workflow. It's an opportunity for programmers who already spend a lot of time on the command line to join forces with writers and write daily. If you decide to write _and_ push code daily, you can, but the purpose of Gaman is to get you to publish something in response to a prompt I provide, daily for the month of December.

Gaman. Stoic endurance. 

You can't compare suffering. Suffering is like a gas. It fills whatever container it's in. Gaman is one way to respond to difficult circumstances.

> Showing gaman is seen as a sign of maturity and strength. Keeping your private affairs, problems and complaints silent demonstrates strength and politeness as others have seemingly larger problems as well. If a person with gaman were to receive help from someone else, they would be compliant; not asking for any additional help and voicing no concerns.[19](https:en.wikipedia.org/wiki/Gaman_(term)

At the start of 2013, when I arrived in Brooklyn, I started seeing why I'd had such a hard year in 2012. I wasn't the only one. Lots of people were (and are) couch surfing. Lots of people couldn't (and can't) make rent. 

In Oakland, it was the same story. People living on the streets. People on food stamps. People you wouldn't think are on food stamps --- are on food stamps. Many of you reading this are Americans, and you know that America is in a downward spiral. 

Stoicism was born of Rome's downward spiral. The Stoics remind us: it could always be worse. There are times when the best you can do is gaman. Take Gaman as a chance to commit your most miserable shit. Why miserable? Why shit? My best work comes out of just getting shit down. Not trying for poetry.

Or take it, as Mike Wong put it, "as encouragement of soulful reflection and work to be done offline". 

Or take it as a month of permission for creative destruction. 

Or take it, as someone who popped into IRC put it, as your chance for "continuous improvement".

Take it as a chance to see the year for what it was, so you can leave it behind. 

#### Format

+ Gaman starts 1 December 2013, ends on 31 December 2013.
+ If you're on the list, you'll get prompts to your inbox. I'll also post them here at [gaman](/gaman).
+ Commit to committing for the month in IRC on channel #gaman (Freenode) -- IRC instructions [here](/irc).
+ You get 31 prompts. One for each day of the month.
+ You respond to the prompt, then commit using Git.
+ You post a link to the day's response in IRC -- #gaman

#### To participate

1. Find a commit buddy. Someone in your real life who is willing to commit each day of the month alongside you
1. If you haven't already, learn Git basics
1. Once you've made your daily commit, publish it to #gaman in IRC
1. Sign up in #gaman on IRC - leave your name and site name. Please leave it in markdown format, as I'm hand-rolling this
1. To visit as a guest, try this. You might think of something more memorable than 'guest 12345'

<iframe src="https://kiwiirc.com/client/irc.freenode.net/?nick=Gaman|?#gaman" style="border:0; width:100%; height:450px;"></iframe>

Here's how to leave your name and site in markdown:

	1. [Your Site Name](Your Site Address)

If I don't respond immediately in IRC, I'll see your message next time I attach tmux -- (unless it crashes, which is rare). 

So I'll get your message -- it's sort of an IRC answering machine. Bear in mind it's public and on the record. 

#### Are private repos ok?

Yes. Of course, feel free to publish to a private repo. [Gitlab](http://gitlab.com) has 'em free, though I haven't yet used one.

Mike Wong's done the end of year project thing since #best09. On the subject of buddies and private repos, Mike Wong said,

>19:06 < mike_w> gwenbell: it's easy enough to understand. i was coming with the approach of having the offline buddy to assist with the challenge of
showing up day to day to do the prompts and answer them honestly. and then for myself, getting a private repo locked up on gitlab and #gaman crew being my buddies. 

#### What about x?

What about x? was the most often asked question I got when I ran end of year projects #reverb10 and #best09. 

My response was and is: feel free to fork this (in programmer-ese) and do it how you want.

If you want to use the prompts to 

	git push origin notebook

you can. Of course!

#### Winter Reading

+ [What's Wrong with Being Number 2?](https://www.adbusters.org/magazine/93/whats-wrong-being-no-2.html)
+ A Guide to the Good Life: The Ancient Art of Stoic Joy, [William B. Irvine](http://boingboing.net/2010/11/01/twenty-first-century-4.html)
+ [On the Shortness of Life, Seneca](http://www.us.penguingroup.com/nf/Book/BookDisplay/0,,9781101651186,00.html?On_the_Shortness_of_Life_Seneca)

Gwen

I dedicate this year's end of year project to my grandparents, Nunu and Bub, without whom none of this would be possible. From them I learned, when you're going through hell, gaman.

