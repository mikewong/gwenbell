### Git &#8800; GitHub

You should only experience one thing when you see the 'fail unicorn' on GitHub: No big deal. I'll pull from Gitboria or Gitlab, or some other remote. That's what gives Git part of its power; you don't have to rely on GitHub for it.

That said. If GitHub goes down and you don't have a copy of your work on your local machine...and GitHub's the only remote you're using...that work is _gone_.

If you're using distributed software on a centralized system, your work is at risk. 

It defeats the purpose of Git to only use it in conjunction with GitHub.

***

I got an email from a reader right after she bought [Git Commit](http://git.gwenbell.com). She said she'd bought my guide to using GitHub.

She can be forgiven for thinking Git is synonymous with GitHub. GitHub has spent a bundle (or at least a few dollars on stickers) marketing itself as the one stop shop for those of us using Git. Here's the thing. Git's not GitHub.

I've seen a high number of "DDOS attacks" coming out of GitHub the past few weeks. Today I saw they're hiring ["spamurai"](https://twitter.com/erebor/status/364505328370860033). About six months ago they went on a hiring spree of attractive young women with weak commit records. And if you ask GitHub about any of this, as I have, they might cc your message to everyone who works there, locking arms, saying they've got a "flat company structure" thing going on. See also [Valve](http://www.gamesindustry.biz/articles/2013-07-08-valves-flat-structure-leads-to-cliques-say-ex-employee).

If you have a repo on GitHub, and you don't have a copy on your local machine, and GitHub goes down in a ["DDOS attack"](http://techcrunch.com/2013/03/10/github-hit-with-another-ddos-attack-second-in-two-days-and-major-service-outage/) and, with no explanation, disappears, you're screwed.

Lucky for you, Git is designed for you to have multiple repos. That's part of Git's power. If you're only pushing to GitHub you're learning some poor habits, which is fine (perhaps) when you're just starting. But Linus Torvalds, the dude who created Git, would, I think, [warn you off using](https://github.com/torvalds/linux/pull/17#issuecomment-5654674) it for anything other than hosting your site. There are no pull requests in Git. That's unique to GitHub. (No, the irony is not lost on me that he made that comment on a GitHub thread.)

It makes your life easier, but over-reliance could get you like a roach caught in a door frame when someone slams the door in the night.

Don't get caught in a door frame! Here are two GitHub alternatives. 

###GitLab

[GitLab](http://gitlab.com) is an open source web-based git management application based out of the Netherlands. Instead of pull requests, you get merge requests. And you have to do them the hard way. You get a wall to talk with team members. You can host free private repos. With GitHub, you cannot.

###Gitboria

[Gitboria](http://gitboria.com) is a GitLab instance only accessible through CJDNS. I've found it to be faster and more secure than both GitHub and GitLab. I'm out in the woods, and SSH can be unreliable. 

If you're not already using GitLab or Gitboria (first get on [Hyperboria](https://raw.github.com/cjdelisle/cjdns/master/doc/Whitepaper.md)), you may want to do that sooner than later.

Remember. Check your remotes with 

	% git remote -v

To add a new remote to your repo, do

	% git remote add [remote name][remote location]

And to remove it

	% git remote rm [remote name][remote location]

Easy. 

And could save you when the unicorns.

