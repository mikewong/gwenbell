### What Begins with G? Gnome, Gimp and GUIs

<mark>Ok. 24 hours later, I'm back to dwm. That was a fun stepping back in time, but I missed dwm so bad! Alright then, back to [dwm](http://dwm.suckless.org) and [scrotting](https://en.wikipedia.org/wiki/Scrot) then. Guess I'll just have to suffer through on the Gimp front, unless one of you has some better ideas. Ha!</mark>

<img src="public/images/gnome-gimp.png" width="100%">

It's been a year since I last played with Gnome. I pacman -S'ed it yesterday because I wanted to edit a super hot photo I took of somebody and it was driving me berzerk trying to make edits in Gimp with dwm.

So, here we are. [Gnome's come a long way in a year!](https://help.gnome.org/misc/release-notes/3.10/)

#### Some Gnome Benefits

+ rounded corners. I know this is a small thing, but it pleases the queen
+ graphics, graphics, graphics. If you're using Arch, give it a go
+ it's called gnome, which is cute, right
+ it works. It's not crashy; it works
+ Gimp! It works better in Gnome than with dwm/Terminology

#### Some Gnome Drawbacks

+ It's not as terminal-focused
+ No dwm...though you might not miss it 
+ Doesn't look as hardcore from afar; but who among us is vain enough to care about that?
