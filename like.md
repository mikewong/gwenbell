### You Are Your Word

I'm going to tell you the cheapest, highest impact technology money can buy. For like, free. 

First, I'm going to tell you a story.

This story features my grandmother Nunu and [Ev Bogue](http://evbogue.com), the person I gallivant around the globe with.

So we're visiting, standing in her kitchen, and she goes,

"Gwen honey, can I give you two some feedback?"

And I felt pretty nervous, because when my grandmother goes "Gwen, honey," I know she's for real. (And if she calls me Gwendolyn Jane, I know she's pissed. Or, at least that was true growing up.)

So I say, "urm....ok!"

"The two of you say something an awful lot. Are you aware of what it is?"

Oh! A puzzle. Is she going to ding me for my dangling particles?

"You say 'like' and 'y'know' a lot."

"A lot?" I gulp.

"Every few words," she said.

***

### FILLER

I hate filler. I work hard to cut the crap, cut the fat, and here I was filling my sentences with two empty words: like and y'know.

The first, although I'm off Fxxxbook, it should be clear. That's where we've gotten 'like'. Yes, it's an actual word. But when I hear it in converation, and when I _used_ it in conversation, it was an empty calorie. It has zero impact on the listener. It is pure filler.

You know, upon reflection, was to encourage the listener to come along with the story I told. It's another way of saying 'you get me? You follow?'

But the pernicious one is the word, 'like'. If you listen to the conversations around you you will hear it in conversation, sometimes as many as every three words.

### THE SOLUTION

Here's the cheap solution. 

1. Find a pack of playing cards.
1. They must be red.
1. Find the Joker.
1. Each time you or the person with whom you share space uses the word 'like', flag them.
1. Flag yourself. Flash the red joker when you catch yourself using filler.

### THE HARDER SOLUTION

Quit Fxxxbook. 

I was already off, and still overused the word 'like'. If you're on there, surrounded by 'like', you'll probably have a harder time. 

Commit to getting off like and cut the filler.

You are your word.

Do you want that word to be **like**?
