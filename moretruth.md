### More Truth

I'm at La Costa sharing a carne asada burrito. Everything on it. A large horchata is $3. It tastes... remember when you used to let your cinnamon toast crunch go to mush, that watery milk left at the bottom of the bowl. That's how horchata tastes.

I'm writing this into my notebook. It's a Palamino. I'm writing with a Sakura Micron .2.

It's not yet noon. I've free written. Gotten brunch-lunch. And created a submarine in urbit.

Yesterday I pushed EB hard to publish a piece he's been sitting on. He published and password protected an encrypted version to those who requested. Within minutes of sending the letter he had dozens of requests. He published, then went to the bathroom, and returned to an inbox full of requests. It was as he said: the web, the dying net,is not kind to radical ideas from edge dwellers right now.

Still. The truth is all I can stomach and it strikes me people tolerate an inconvenience to access it right now.

When we passed notes in class we assumed they might get intercepted by a meddlesome teacher. So we spoke in code. We changed up the routes. We folded the paper in patterns.

We're there again.

***

Woke up to a note from a reader. She bought _Terminal_ in the same message in which I gave her an encrypted password-protected version of my first long-form poem in years. (Yes, I started this trend, by accident.) She asked if _Terminal_ was still available.

She read all of it. Then sent this:

> **Blown Away** Just wanted to let you know, they are without a doubt, some of your BEST work. The poem and Terminal  ~ Art at its finest ~ The poem made me take a look at reality, and Terminal lifted me up to face it. I'm glad I got to read them in that order. I loved Terminal. I never expected to experience your sense of humor in that piece. Expectations exceeded! I'll be reading them both again soon. And yes, I do feel free after moving on from Tweeting. We all need more truth.

And sometimes, the truth has to be passed in a note.

A primitive form of self-protection.

For ideas-in-process.

For unpopular ideas.

For licks at 12:00.05 on The Glitch Mob's Drink the Sea Part 2.

For tiny tendrils to take root.

As long as I am on this planet, I am devoted to those tiny tendrils.

May they, and that, and this work continue to serve those tendrils.

Those note passers.

Those brave enough

To tell

The truth.
