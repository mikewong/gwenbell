### The Underwhelm and the Overwhelm: Two Impediments on Your Path to Becoming a Programmer (& How to Overcome Them)

> "Thing is when you build a boat, when it's finished, you can go 'there, look at that. It's done.' When you program, you can't point to a system and say, 'there, 'it's done.' There's nothing to see for your work." - Dominic Tarr, October 2013

You might look at the world of programming and think, "I'd hate that," because it looks, in an odd way, both underwhelming _and_ overwhelming. I'd like to present you with an argument to encourage you to program anyway.

**The Underwhelm**

As you learn to program, if you're the type who grew up loving show and tell, enjoyed seeing gold stars on the top of your work, and thrived on public acknowledgment...you might find programming underwhelming at first.

As Dominic pointed out, you build a boat, there is a boat to prove you built it. You can point to the craftsmanship and tell stories about what a pain in the ass some finer point was. In programming, there is no boat.

Let's take the non-boat Git. [Git](http://git.gwenbell.com) is a difficult system to visualize. When I [speak on the subject](http://blog.flatironschool.com/post/52299707175/guest-speaker-gwen-bell), I start with a Voldemort metaphor. His power comes of being capable of breaking up his soul into little pieces and distributing them. So it goes with Git.

Here's how to counteract the underwhelm 

1. Get your hands on the keyboard. Don't just watch talks about programming. Try. 
1. Do it for 30 days. A month with daily Git commits, you'll start to see the magic. Or you won't. But you'll never know if you never try for yourself.1. If you program full-time and you're underwhelmed because it's all looking the same, take on a task totally different than your current job title suggests. Install a new operating system if you usually spend all day in JavaScript.
1. Appreciations! Practice appreciating aloud the small, invisible, things that you and the other programmers in your life create. Including messes!

**The Overwhelm**

"What's that?" he said. "A T L A," I repeated, "a three letter acronym."

Yes, it's meta to describe a three letter acronym with three letters. That's the point.

When you hear the phrase "I'm just gonna fire up an Arch instance on my VPS and then SSH in to Vim this change," you've just heard two TLAs. Three letter acronyms kill conversations, alienate listeners and produce overwhelm in someone new to a field. When you're a beginner, TLAs are an impediment.

Some programmers (not all, not by any stretch) use TLAs to intimidate and overwhelm Outsiders, so that Outsiders decide not to join their party. 

DON'T LET TLAS RUIN YOUR PARTY.

Here's how to counteract the overwhelm

1. Ask! Ask what each letter stands for in the three letter acronym. I can't _tell_ you the number of big shot programmers I've stumped by asking: what does that stand for?
1. Puzzle it out for yourself before you ask. What _might_ a CDN do?

Last night I learned that GRUB is a TLA+1. It stands for GRand Unified Bootloader. If some Stallmanesque programmer attempts to TLA 1UP you, you now have an ace up your sleeve. My guess is that person will not know what GRUB stands for. And if they do, hang out with them and ask a bunch of annoying questions. Programmers play like they don't want to be annoyed by you. 

Annoy them anyway. It's how you'll learn. 

Moreover, it's how _they'll_ learn.

***

I've just been thinking about an operating system designed with women in mind. I've felt the first glimpses of it using a tiling window manager; it works the way my brain does. 

What would it be like to build the operating system I want to use each day, from the ground up? Do I want to be one of the one who helps design it? The more I do this work, the more I see the importance of lowering the barriers to not just _entry_ but _engagement_ for developers. 

Maybe just asking these questions and addressing the overwhelm and the underwhelm is a start.


